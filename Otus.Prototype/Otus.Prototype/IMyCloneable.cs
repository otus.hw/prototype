﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Otus.Prototype
{
    public interface IMyCloneable<T> where T : class
    {
        T Copy();
        //string GetInfo();
    }
}

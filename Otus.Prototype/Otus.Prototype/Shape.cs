﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Prototype
{
    /// <summary>
    /// Абстрактная фигура с центром
    /// </summary>
    public class Shape : IMyCloneable<Shape>, ICloneable
    {
        public Point Point { get; private set; }

        public Shape(Point point)
        {
            Point = point;
        }

        public void PrintCenter()
        {
            Console.WriteLine($"Center point X: {Point.X} Y: {Point.Y}");
        }

        public virtual Shape Copy()
             => new Shape(Point);

        public virtual string GetInfo()
            => $"Shape. Point x: {Point.X}, y: {Point.Y} ";

        public virtual object Clone()
            => Copy();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Prototype
{
    /// <summary>
    /// Эллипсо́ид с полуосями Radius, Radius2, Radius3
    /// </summary>
    public class Ellipsoid : Ellipse, IMyCloneable<Shape>
    {
        public int Radius3 { get; private set; }

        public Ellipsoid(Ellipse ellipse, int radius) : base(ellipse, ellipse.Radius2)
        {
            Radius3 = radius;
        }

        public void LargeRadius3()
            => Radius3 += 5;

        public override Shape Copy()
            => new Ellipsoid(this, Radius3);

        public override string GetInfo()
            => "Ellipsoid. Radius: " + this.Radius + ", Radius2: " + this.Radius2 + ", Radius3: " + this.Radius3;

        public override object Clone()
            => Copy();

    }
}

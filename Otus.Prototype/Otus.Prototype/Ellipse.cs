﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Prototype
{
    /// <summary>
    /// Элипс на базе окружности с радиусом Radius(большая ось) и радиусом малой оси Radius2 
    /// </summary>
    public class Ellipse : Circle, IMyCloneable<Shape>
    {
        public int Radius2 { get; private set; }

        public Ellipse(Circle circle, int radius2) : base(circle, circle.Radius)
        {
            Radius2 = radius2;
        }

        public void LargeRadius2()
            => Radius2 += 5;

        public override Shape Copy()
            => new Ellipse(this, Radius2);

        public override string GetInfo()
            => "Ellipse. Radius: " + this.Radius + ", Radius2: " + this.Radius2;

        public override object Clone()
            => Copy();

    }
}

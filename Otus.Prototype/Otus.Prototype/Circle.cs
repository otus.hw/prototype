﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Prototype
{
    /// <summary>
    /// Окружность с заданным радиусом Radius
    /// </summary>
    public class Circle : Shape, IMyCloneable<Shape>
    {
        public int Radius { get; private set; }
        public Circle(Shape shape, int radius): base (shape.Point)
        {
            Radius = radius;
        } 

        public void LargeRadius()
            => Radius += 5;

        public override Shape Copy()
            => new Circle(this, Radius);

        public override string GetInfo()
            => "Circle. Radius: " + this.Radius;

        public override object Clone()
            => Copy();

    }
}

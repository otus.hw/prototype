﻿using System;

namespace Otus.Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point(1, 3);

            Shape shape = new Shape(point);

            Circle circle = new Circle(shape, 4);

            Ellipse ellipse = new Ellipse(circle, 3);

            Ellipsoid ellipsoid = new Ellipsoid(ellipse,2);

            var anotherEllipse3D = ellipsoid.Copy();

            Console.WriteLine($"        ellipsoid {ellipsoid.GetInfo()}");
            Console.WriteLine($" anotherEllipsoid {anotherEllipse3D.GetInfo()}");

            ellipsoid.LargeRadius3();
            ellipsoid.LargeRadius2();
            ellipsoid.LargeRadius();

            Console.WriteLine(Environment.NewLine+"After change ellipsoid" +Environment.NewLine);

            Console.WriteLine($"        ellipsoid {ellipsoid.GetInfo()}");
            Console.WriteLine($" anotherEllipsoid {anotherEllipse3D.GetInfo()}");

            Console.WriteLine();


            Console.WriteLine($" ellipse {ellipse.GetInfo()}");

            Console.WriteLine($" circle {circle.GetInfo()}");

            Console.WriteLine();

            var cloneEllipsoid = (Ellipsoid)ellipsoid.Clone();

            Console.WriteLine($"cloneEllipsoid {cloneEllipsoid.GetInfo()}");

            ellipsoid.LargeRadius3();
            ellipsoid.LargeRadius2();
            ellipsoid.LargeRadius();

            Console.WriteLine(Environment.NewLine + "After change ellipsoid" + Environment.NewLine);

            Console.WriteLine($"        ellipsoid {ellipsoid.GetInfo()}");
            Console.WriteLine($"cloneEllipsoid {cloneEllipsoid.GetInfo()}");





        }
    }
}
